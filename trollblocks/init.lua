TROLLBLOCKSRAYON = 1
CHATDEBUGTROLL = false

minetest.register_chatcommand("setTrollRayon", {
    params = num_blocs,
    description = "Set num_blocs the rayon of jumps for trolling blocs",
    func = function( _,num_blocs)
        if not num_blocs or num_blocs=="" then
            minetest.chat_send_all("Valeur manquante")
            return false, "Trollblocks: RAYON has nil value " .. num_blocs
        else
            --minetest.chat_send_all("Rayon passe a " .. num_blocs)
            TROLLBLOCKSRAYON = tonumber(num_blocs)
            return true, "Trollblocks: RAYON has been set to " .. num_blocs
        end

    end,
})

minetest.register_chatcommand("setTrollDebug", {
    params = isdebug,
    description = "Set debug chat messages on/off",
    func = function( _,isdebug)
        if isdebug=="" then
            minetest.chat_send_all("Valeur manquante")
            return false, "Trollblocks: Debug has nil value " .. isdebug
        else
            --minetest.chat_send_all("Rayon passe a " .. num_blocs)
            CHATDEBUGTROLL = isdebug
            return true, "Trollblocks: CHATDEBUG has been set to " .. isdebug
        end

    end,
})

minetest.register_on_dignode(
    function(pos, oldnode, digger)
        ---- UTILITAIRE ----
        -- Copie une table --
        function deepcopy(orig)
            local orig_type = type(orig)
            local copy
            if orig_type == 'table' then
                copy = {}
                for orig_key, orig_value in next, orig, nil do
                    copy[deepcopy(orig_key)] = deepcopy(orig_value)
                end
                setmetatable(copy, deepcopy(getmetatable(orig)))
            else -- number, string, boolean, etc
                copy = orig
            end
            return copy
        end
        -- FIN UTILITAIRE -
        -- DECLARATIONS --
        local found = false
        --local testpos = deepcopy(pos)
        local move_block_done = false
        local good_positions = {}
        local new_pos = deepcopy(pos)
        local x_max = pos.x+TROLLBLOCKSRAYON
        local y_max = pos.y+TROLLBLOCKSRAYON
        local z_max = pos.z+TROLLBLOCKSRAYON
        local x_min = pos.x-TROLLBLOCKSRAYON
        local y_min = pos.y-TROLLBLOCKSRAYON
        local z_min = pos.z-TROLLBLOCKSRAYON
        print("Initial pos:" .. minetest.pos_to_string(pos) )
        bloc_type = minetest.get_node(pos)
        if CHATDEBUGTROLL then
            minetest.chat_send_all("Type de bloc miné: " .. bloc_type.name)
        end

        for i = x_min, x_max do
            new_pos.x = i
            for j = y_min, y_max do
                new_pos.y = j
                for k = z_min, z_max do
                    new_pos.z = k
                    bloc_candidat = minetest.get_node(new_pos)
                    if CHATDEBUGTROLL then
                        minetest.chat_send_all("Type de bloc autour: " .. bloc_candidat.name)
                    end
                    if bloc_candidat.name == "air" or bloc_candidat.name == "default:water_source" or bloc_candidat.name == "default:water_flowing" then
                        print("Good pos:" .. minetest.pos_to_string(new_pos) )
                        table.insert(good_positions,deepcopy(new_pos))
                    end
                end
            end
        end

        for index, valeur in pairs(good_positions) do
            print(index, minetest.pos_to_string(valeur) )
        end

        print("Longueure de la table: " .. #good_positions)
        local position_chooser = math.random(1,#good_positions-1)

        print("Noumbeure aleatoire: " .. position_chooser )
        local ancienne_position = deepcopy(pos)
        local position_choisie = pos
        position_choisie.x = good_positions[position_chooser].x
        position_choisie.y = good_positions[position_chooser].y+1
        position_choisie.z = good_positions[position_chooser].z

        print(position_choisie.x)
        print(position_choisie.y)
        print(position_choisie.z)
        print("Choix aleatoire: " .. minetest.pos_to_string(position_choisie) )
        minetest.set_node(position_choisie, {name=oldnode.name})
    end
)
